let headerObj = [
    {
        id: "0",
        name: "Currency"
    },
    {
        id: "1",
        name: "Symbol"
    },
    {
        id: "2",
        name: "Old"
    },
    {
        id: "3",
        name: "New"
    },
    {
        id: "4",
        name: "Diff"
    },
    {
        id: "5",
        name: "Index"
    }

];
let valuesArray = [
    {
        "id": "0",
        "values": [
            "Bitcoin", "BTC", "9515.40", "9910.34"
        ]
    },
    {
        "id": "1",
        "values": [
            "Ethereum", "ETH", "475.17", "471.35"
        ]
    },
    {
        "id": "2",
        "values": [
            "Litecoin", "LTC", "88.96", "95.23"
        ]
    }
];
let refreshValuesArray = [
    {
        "id": "0",
        "values": [
            "Bitcoin", "BTC", "9785.40", "9915.34"
        ]
    },
    {
        "id": "1",
        "values": [
            "Ethereum", "ETH", "485.17", "471.35"
        ]
    },
    {
        "id": "2",
        "values": [
            "Litecoin", "LTC", "95.46", "92.29"
        ]
    },
    {
        "id" : "3",
        "values": [
            "Dash", "DASH", "628.06", "651.01"
        ]
    },
    {
        "id" : "4",
        "values": [
            "Bitcoin Cash", "BCH", "1631.58", "1562.33"
        ]
    }
];
let newValuesArray = [];
let firstAdd = {};
firstAdd.id = "3";
firstAdd.values = ["Dash", "DASH", "631.06", "626.01"];
let secondAdd = {};
secondAdd.id = "4";
secondAdd.values = ["Bitcoin Cash", "BCH", "1634.58", "1565.33"];
newValuesArray.push(firstAdd,secondAdd);