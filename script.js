$( document ).ready(function() {

    populateHeader(headerObj);
    (function () {
        for(let i = 0; i < newValuesArray.length; i++){
            populateAddSelectors(newValuesArray[i].id, newValuesArray[i].values[0]);
        }
    })();

//Function for populating Header of table
    function populateHeader(array) {
        for(let i = 0; i < array.length; i++) {
            $("#header").append("<th>" + array[i].name + "</th>");
        }
        populateBody(valuesArray);
    }
//Function for populating Body of table
    function populateBody(array) {
        for(let i = 0; i < array.length; i++) {
            populateRow(array[i]);
        }
    }
//Function for populating row
    function populateRow(item) {
        let text = "<tr id=a" + item.id + ">";
        for(let i = 0; i < item.values.length; i++) {
            text += "<td>" + item.values[i] + "</td>";
        }

        text += diffAndIndex(item.values[3], item.values[2]) + "</tr>";
        $("#example tbody").append(text);
        populateRemoveSelectors(item.id, item.values[0]);
    }

//Function adding table parts
    function diffAndIndex(value1, value2){
        let calc = calculateDiff(value1, value2);
        if(calc > 0){
            return "<td>" + calc + "</td>" + "<td><span class='higher'>&uarr;</span></td>";
        } else {
            return "<td>" + calc + "</td>" + "<td><span class='lower'>&darr;</span></td>";
        }
    }
//Function for calculating diff
    function calculateDiff(value1, value2){
        return ( value1 - value2 ).toFixed(2);
    }
//Function for removing row
    function removeOneRow(id) {
        $("#a" + id).remove();
    }
// Functions for populating selectors
    function populateAddSelectors(key, value) {
        $('#selectCoinsAdd')
            .append($('<option>', { value : key })
                .text(value));
    }
    function populateRemoveSelectors(key, value){
        $('#selectCoinsRemove')
            .append($('<option>', { value : key })
                .text(value));
    }

//Event for adding new row
    $("#addNew").click( function() {
        let selectorValue = $('#selectCoinsAdd').val();
        let oneObject = findJSONItem(newValuesArray, 'id', selectorValue);
        if(oneObject){
            valuesArray.push(oneObject);
            removeJSONItem(newValuesArray, 'id', selectorValue);
            populateRow(oneObject);
            $('#selectCoinsAdd option:selected').remove();
        }
});
//Event for removing row from table
    $("#removeOne").click( function() {
        let selectorValue = $('#selectCoinsRemove').val();
        let selectorText = $('#selectCoinsRemove option:selected').text();
        let oneObject = findJSONItem(valuesArray, 'id', selectorValue);
        if(oneObject){
            newValuesArray.push(oneObject);
            removeJSONItem(valuesArray, 'id', selectorValue);
            removeOneRow(selectorValue);
            populateAddSelectors(selectorValue, selectorText);
            $('#selectCoinsRemove option:selected').remove();
        }
    });
//Event for refreshing some table data
    $("#refresh").click( function() {
        $.each(refreshValuesArray,function(i,v){
            let oneObject = findJSONItem(valuesArray, 'id', i.toString());
            if(oneObject){
                let x = valuesArray.indexOf(oneObject);
                if(x !== -1){
                    valuesArray[x] = refreshValuesArray[i];
                    appendNewData(x, refreshValuesArray[i].values)
                }
            }
        });
    });
//After refresh update one row fields by id
    function appendNewData(id, newValues){
        $("#a" + id).find('td:eq(2)').html(newValues[2]);
        $("#a" + id).find('td:eq(3)').html(newValues[3]);
        let calc = calculateDiff(newValues[3],newValues[2]);
        $("#a" + id).find('td:eq(4)').html(calc);
        if(calc > 0) {
            $("#a" + id).find('td:eq(5)').html("<span class='higher'>&uarr;</span>");
        } else {
            $("#a" + id).find('td:eq(5)').html("<span class='lower'>&darr;</span>");
        }
    }

    function removeJSONItem(arr, key, value) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key] === value) {
                delete arr[i];
                arr.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    function findJSONItem(arr, key, value) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key] === value) {
                return (arr[i]);
            }
        }
    }
});